/*
Author  : Laurence P. Abanes
Date    : Dec 7, 2018
Project : Ordered List Arrays
*/

#include <stdio.h>
#define size 5

void enQueue();
void deQueue();
void display();

int itemList[size], head = -1, tail = -1;

int main() {
    int selectFunction;
    printf("\n\nPlease choose function: \n[1] Add Item \n[2] Delete Item \n[0] Exit \n\n");
    scanf("%d", &selectFunction);

	while (selectFunction !=0){
		switch (selectFunction) {
			case (1):
				enQueue();
				display();
				break;
      
			case(2):
				deQueue();
				display();
				break;

			default:
				printf("\n[Error] Invalid Selection");
		}
		printf("\n\nPlease choose function: \n[1] Add [2] Delete [0] Exit\n");
		scanf("%d", &selectFunction);
	}
}

void enQueue(){
  if (tail == size -1){
    printf("\nArray is full!");
  }
  else {
    int value;
	  printf("\nInput item to add: ");
	  scanf("%d", &value);
	
	if (head == -1){
        head = 0;
	}
    
	tail++;
    itemList[tail] = value;
    printf("Adding %d ...", value);
  }
}

void deQueue() {
  if (head == -1) {
  }
  else {
    int i;
    printf("\nRemoved: %d", itemList[head]);
    head++;
    if (head > tail){
      head = tail = -1;
    }
  }
}

void display(){
  if (tail == -1){
    printf("\nArray is empty!");
  }
  else{
    int i;
    printf("\n\nThe elements of Array: \n");
    for (i=head; i <= tail; i++){
      printf("%d\t", itemList[i]);
    }
  }
}


